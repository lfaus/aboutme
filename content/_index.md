---
title: "Something About Me"
draft: false
---

<div class="bg-white">
  <div class="relative bg-gray-800 pb-32">
    <div class="absolute inset-0">
      <img class="h-full w-full object-cover" src="https://images.unsplash.com/photo-1525130413817-d45c1d127c42?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1920&q=60&&sat=-100" alt="">
      <div class="absolute inset-0 bg-gray-800 mix-blend-multiply" aria-hidden="true"></div>
    </div>
    <div class="relative mx-auto max-w-7xl py-12 px-10 sm:py-12 lg:px-8">
      <div class="mb-8 h-12">
        <img class="object-contain h-10" src="images/gitlab-logo-200.png"/>
      </div>
      <h1 class="text-4xl font-bold tracking-tight text-white md:text-5xl lg:text-7xl">Lee Faus</h1>
      <p class="mt-3 max-w-3xl text-5xl text-gray-300">Global Field CTO</p>
    </div>
  </div>
  <section class="relative z-10 mx-auto -mt-40 max-w-7xl px-6 pb-32 lg:px-8">
    <div class="flex items-center justify-center space-x-2">
      <img src="images/headshot.png" alt="headshot" class="relative w-72 h-72 z-30 rounded-full object-cover border-sky-400 ring-8">
    </div>
    <div class="bg-white">
      <div class="mx-auto max-w-7xl px-6 sm:pt-10 lg:py-40 lg:px-8">
        <div class="lg:grid lg:grid-cols-12 lg:gap-8">
          <div class="lg:col-span-5">
            <h2 class="text-5xl font-bold leading-10 tracking-tight text-gray-900">About Me</h2>
            <p class="mt-4 leading-7 text-gray-600 text-2xl">Experienced Global Field CTO with over 20 years of experience in DevOps, open source, and enterprise architecture. Currently mentoring senior executives at G2k and Fortune 500 to help them become more agile and collaborative, while enabling global remote development teams on best practices for DevOps, DevSecOps and GitOps. Previously led a team at GitHub to establish the platform as the de facto for software creation, collaboration and release automation, and served as a Sr. Solutions Architect at Appnovation Technologies working with clients on open source solutions for their cloud and big data strategies. Strong background in continuous delivery, innersource and automation.</p>
          </div>
          <div class="mt-10 lg:col-span-7 lg:mt-0">
            <dl class="space-y-10 mb-8">
              <div>
                <dt class="text-4xl font-semibold leading-7 text-gray-900">About GitLab</dt>
                <dd class="mt-2 text-2xl leading-7 text-gray-600">GitLab is a comprehensive DevSecOps platform that provides a single application for the entire software development lifecycle, from project planning and source code management to CI/CD, monitoring, and security. It enables teams to collaborate efficiently and effectively, streamlining the development process, and delivering better software faster. As an executive, you can use GitLab to manage your company's software development process and ensure that your teams are working together effectively to meet your business and security goals.</dd>
              </div>
            </dl>
            <dl class="space-y-10 mb-8">
              <div>
                <dt class="text-4xl font-semibold leading-7 text-gray-900">Pitch Deck</dt>
                <dd class="mt-2 text-2xl leading-7 text-gray-600"><a href="https://docs.google.com/presentation/d/16OuybKSwzUiPLAO9I8k7R5QZujhM-4Fk087D0D_wqs4/edit?usp=sharing">Google Slides for GitLab Pitch Deck</a></dd>
              </div>
            </dl>
            <dl class="space-y-10 mb-8">
              <div>
                <dt class="text-4xl font-semibold leading-7 text-gray-900">Social Contacts</dt>
                <dd class="mt-2 text-2xl leading-7 text-gray-600"><a href="https://www.linkedin.com/in/leefaus/">LinkedIn</dd>
              </div>
            </dl>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- This example requires Tailwind CSS v3.0+ -->


  
</div>




